# Geolocalización Prixz

_Este es un plugin para guardar y mostrar la localizacion del usuaio en cache_

## Requisitos

_Wordpress 5.7._

### Instalación del plugin

```
Loguearte en wordpress
Ir a la sección plugins
Dar clic en agregar plugins
Ir a la ubicación donde tienes tu zip de geolocalizacion prixz
Seleccionar plugin y subirlo
Finalmente dar clic en el boton instalar
Ir a la seccion de plugins instalados y dar clic en activar

```

## Autor

_Victor Segura_
