<?php

/*
Plugin Name: Geo Prixz
Description: Localiza un usuario y guarda el resultado en una cookie, adicional a ello, imprime el resultado en el pie de pagina
Version: 1.0
Author: Victor Segura
*/

/**
 * Fecha ultima act: 23-06-2021
 *
 */
 
 if (!defined('ABSPATH')) die();

/**
 * Fecha ultima act: 22-06-2021
 *
 * Sin parametros
 * Consigue la ubicación del usuario, dandonos el país y la ciudad
 */
function getUbicacion() {

    $user_ip = getenv('REMOTE_ADDR');
    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
    $country = $geo["geoplugin_countryName"];
    $city = $geo["geoplugin_city"];

    wp_cache_set( "country", $country );
    wp_cache_set( "city", $city );

    echo "<div align='center'> <p>";
    echo "Tu ubicación se encuentra en : <b>". wp_cache_get( "country") ."</b> y Ciudad: <b>". wp_cache_get( "city") . "</b> y han guardados en la cookie";
    echo "</p></div>";
}

/**
 * Fecha ultima act: 22-06-2021
 *
 * Sin parametros
 * Ejecuta la función cuando se ejecuta el pie de pagina
 */
add_action('wp_footer', 'getUbicacion');
